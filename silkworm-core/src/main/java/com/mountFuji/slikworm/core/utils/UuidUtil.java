package com.mountFuji.slikworm.core.utils;

import java.util.UUID;

public class UuidUtil {

    public static String uuid() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}